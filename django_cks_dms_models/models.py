from django.db import models


class CksFiles(models.Model):
    docentry = models.IntegerField(
        db_column="DocEntry", primary_key=True
    )  # Field name made lowercase.
    docnum = models.IntegerField(
        db_column="DocNum", blank=True, null=True
    )  # Field name made lowercase.
    period = models.IntegerField(
        db_column="Period", blank=True, null=True
    )  # Field name made lowercase.
    instance = models.SmallIntegerField(
        db_column="Instance", blank=True, null=True
    )  # Field name made lowercase.
    series = models.IntegerField(
        db_column="Series", blank=True, null=True
    )  # Field name made lowercase.
    handwrtten = models.CharField(
        db_column="Handwrtten", max_length=1, blank=True, null=True
    )  # Field name made lowercase.
    canceled = models.CharField(
        db_column="Canceled", max_length=1, blank=True, null=True
    )  # Field name made lowercase.
    object = models.CharField(
        db_column="Object", max_length=20, blank=True, null=True
    )  # Field name made lowercase.
    loginst = models.IntegerField(
        db_column="LogInst", blank=True, null=True
    )  # Field name made lowercase.
    usersign = models.IntegerField(
        db_column="UserSign", blank=True, null=True
    )  # Field name made lowercase.
    transfered = models.CharField(
        db_column="Transfered", max_length=1, blank=True, null=True
    )  # Field name made lowercase.
    status = models.CharField(
        db_column="Status", max_length=1, blank=True, null=True
    )  # Field name made lowercase.
    createdate = models.DateTimeField(
        db_column="CreateDate", blank=True, null=True
    )  # Field name made lowercase.
    createtime = models.SmallIntegerField(
        db_column="CreateTime", blank=True, null=True
    )  # Field name made lowercase.
    updatedate = models.DateTimeField(
        db_column="UpdateDate", blank=True, null=True
    )  # Field name made lowercase.
    updatetime = models.SmallIntegerField(
        db_column="UpdateTime", blank=True, null=True
    )  # Field name made lowercase.
    datasource = models.CharField(
        db_column="DataSource", max_length=1, blank=True, null=True
    )  # Field name made lowercase.
    requeststatus = models.CharField(
        db_column="RequestStatus", max_length=1, blank=True, null=True
    )  # Field name made lowercase.
    creator = models.CharField(
        db_column="Creator", max_length=8, blank=True, null=True
    )  # Field name made lowercase.
    remark = models.TextField(
        db_column="Remark", blank=True, null=True
    )  # Field name made lowercase.
    u_file = models.TextField(db_column="U_File")  # Field name made lowercase.
    u_sha1 = models.CharField(
        db_column="U_SHA1", max_length=40
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "@CKS_FILES"


class CksPresystems(models.Model):
    docentry = models.IntegerField(
        db_column="DocEntry", primary_key=True
    )  # Field name made lowercase.
    docnum = models.IntegerField(
        db_column="DocNum", blank=True, null=True
    )  # Field name made lowercase.
    period = models.IntegerField(
        db_column="Period", blank=True, null=True
    )  # Field name made lowercase.
    instance = models.SmallIntegerField(
        db_column="Instance", blank=True, null=True
    )  # Field name made lowercase.
    series = models.IntegerField(
        db_column="Series", blank=True, null=True
    )  # Field name made lowercase.
    handwrtten = models.CharField(
        db_column="Handwrtten", max_length=1, blank=True, null=True
    )  # Field name made lowercase.
    canceled = models.CharField(
        db_column="Canceled", max_length=1, blank=True, null=True
    )  # Field name made lowercase.
    object = models.CharField(
        db_column="Object", max_length=20, blank=True, null=True
    )  # Field name made lowercase.
    loginst = models.IntegerField(
        db_column="LogInst", blank=True, null=True
    )  # Field name made lowercase.
    usersign = models.IntegerField(
        db_column="UserSign", blank=True, null=True
    )  # Field name made lowercase.
    transfered = models.CharField(
        db_column="Transfered", max_length=1, blank=True, null=True
    )  # Field name made lowercase.
    status = models.CharField(
        db_column="Status", max_length=1, blank=True, null=True
    )  # Field name made lowercase.
    createdate = models.DateTimeField(
        db_column="CreateDate", blank=True, null=True
    )  # Field name made lowercase.
    createtime = models.SmallIntegerField(
        db_column="CreateTime", blank=True, null=True
    )  # Field name made lowercase.
    updatedate = models.DateTimeField(
        db_column="UpdateDate", blank=True, null=True
    )  # Field name made lowercase.
    updatetime = models.SmallIntegerField(
        db_column="UpdateTime", blank=True, null=True
    )  # Field name made lowercase.
    datasource = models.CharField(
        db_column="DataSource", max_length=1, blank=True, null=True
    )  # Field name made lowercase.
    requeststatus = models.CharField(
        db_column="RequestStatus", max_length=1, blank=True, null=True
    )  # Field name made lowercase.
    creator = models.CharField(
        db_column="Creator", max_length=8, blank=True, null=True
    )  # Field name made lowercase.
    remark = models.TextField(
        db_column="Remark", blank=True, null=True
    )  # Field name made lowercase.
    u_dbdescription = models.CharField(
        db_column="U_DBDescription", max_length=254, blank=True, null=True
    )  # Field name made lowercase.
    u_erpidentifier = models.CharField(
        db_column="U_ErpIdentifier", max_length=36, blank=True, null=True
    )  # Field name made lowercase.
    u_presystemname = models.CharField(
        db_column="U_PreSystemName", max_length=100, blank=True, null=True
    )  # Field name made lowercase.
    u_workgroup = models.CharField(
        db_column="U_Workgroup", max_length=100, blank=True, null=True
    )  # Field name made lowercase.
    u_active = models.CharField(
        db_column="U_Active", max_length=3, blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "@CKS_PRESYSTEMS"


class CksThumbnails(models.Model):
    docentry = models.IntegerField(
        db_column="DocEntry", primary_key=True
    )  # Field name made lowercase.
    docnum = models.IntegerField(
        db_column="DocNum", blank=True, null=True
    )  # Field name made lowercase.
    period = models.IntegerField(
        db_column="Period", blank=True, null=True
    )  # Field name made lowercase.
    instance = models.SmallIntegerField(
        db_column="Instance", blank=True, null=True
    )  # Field name made lowercase.
    series = models.IntegerField(
        db_column="Series", blank=True, null=True
    )  # Field name made lowercase.
    handwrtten = models.CharField(
        db_column="Handwrtten", max_length=1, blank=True, null=True
    )  # Field name made lowercase.
    canceled = models.CharField(
        db_column="Canceled", max_length=1, blank=True, null=True
    )  # Field name made lowercase.
    object = models.CharField(
        db_column="Object", max_length=20, blank=True, null=True
    )  # Field name made lowercase.
    loginst = models.IntegerField(
        db_column="LogInst", blank=True, null=True
    )  # Field name made lowercase.
    usersign = models.IntegerField(
        db_column="UserSign", blank=True, null=True
    )  # Field name made lowercase.
    transfered = models.CharField(
        db_column="Transfered", max_length=1, blank=True, null=True
    )  # Field name made lowercase.
    status = models.CharField(
        db_column="Status", max_length=1, blank=True, null=True
    )  # Field name made lowercase.
    createdate = models.DateTimeField(
        db_column="CreateDate", blank=True, null=True
    )  # Field name made lowercase.
    createtime = models.SmallIntegerField(
        db_column="CreateTime", blank=True, null=True
    )  # Field name made lowercase.
    updatedate = models.DateTimeField(
        db_column="UpdateDate", blank=True, null=True
    )  # Field name made lowercase.
    updatetime = models.SmallIntegerField(
        db_column="UpdateTime", blank=True, null=True
    )  # Field name made lowercase.
    datasource = models.CharField(
        db_column="DataSource", max_length=1, blank=True, null=True
    )  # Field name made lowercase.
    requeststatus = models.CharField(
        db_column="RequestStatus", max_length=1, blank=True, null=True
    )  # Field name made lowercase.
    creator = models.CharField(
        db_column="Creator", max_length=8, blank=True, null=True
    )  # Field name made lowercase.
    remark = models.TextField(
        db_column="Remark", blank=True, null=True
    )  # Field name made lowercase.
    u_thumbnail = models.TextField(
        db_column="U_Thumbnail"
    )  # Field name made lowercase.
    u_sha1 = models.CharField(
        db_column="U_SHA1", max_length=40
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "@CKS_THUMBNAILS"
